package be.basalte.codectest;

import android.graphics.Color;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.webrtc.EglBase;
import org.webrtc.HardwareVideoEncoderFactory;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements GStreamerCodecCallback {

    static final int TV_HEIGHT = 80;
    static final int TV_HEIGHT_SMALL = 50;

    static final String H264 = "H264";
    static final String AVC = "AVC";
    static final String VP8 = "VP8";
    static final String ENC = "ENC";
    static final String VID = "VID";

    //Common hardware codecs
    static final String EXYNOS_PREFIX = "OMX.Exynos.";
    static final String INTEL_PREFIX = "OMX.Intel.";
    static final String NVIDIA_PREFIX = "OMX.Nvidia.";
    static final String QCOM_PREFIX = "OMX.qcom.";

    //ELLIE hardware codec prefix
    static final String OMXRK = "OMX.RK";

    //LISA hardware codec prefix
    static final String C2IMX = "C2.IMX";

    //Aliases and prefixes
    static final String[] H264_ALIASES = {
        H264,
        AVC
    };
    static final String[] VP8_ALIASES = {VP8};
    static final String[] SOFTWARE_IMPLEMENTATION_PREFIXES = {"OMX.google.", "OMX.SEC."};
    static final String[] HARDWARE_IMPLEMENTATION_PREFIXES = {
        EXYNOS_PREFIX,
        INTEL_PREFIX,
        NVIDIA_PREFIX,
        QCOM_PREFIX,
        OMXRK,
        C2IMX};

    ArrayList<Codec> allCodecs = new ArrayList<>();

    Runnable setUI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI = new Runnable() {
            @Override
            public void run() {
                LinearLayout ll = MainActivity.this.findViewById(R.id.codec_list);

                ll.removeAllViews();

                ll.addView(createTextView(VP8_ALIASES[0] + " HW ENCODING", Color.GRAY, TV_HEIGHT));

                ll.addView(createCodecTextView(
                    CodecType.SYSTEM, VP8_ALIASES, true, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.GSTREAMER, VP8_ALIASES, true, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.WEBRTC, VP8_ALIASES, true, true
                ));

                ll.addView(createTextView(VP8_ALIASES[0] + " HW DECODING", Color.GRAY, TV_HEIGHT));

                ll.addView(createCodecTextView(
                    CodecType.SYSTEM, VP8_ALIASES, false, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.GSTREAMER, VP8_ALIASES, false, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.WEBRTC, VP8_ALIASES, false, true
                ));

                ll.addView(createTextView(H264_ALIASES[0] + " HW ENCODING", Color.GRAY, TV_HEIGHT));

                ll.addView(createCodecTextView(
                    CodecType.SYSTEM, H264_ALIASES, true, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.GSTREAMER, H264_ALIASES, true, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.WEBRTC, H264_ALIASES, true, true
                ));

                ll.addView(createTextView(H264_ALIASES[0] + " HW DECODING", Color.GRAY, TV_HEIGHT));

                ll.addView(createCodecTextView(
                    CodecType.SYSTEM, H264_ALIASES, false, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.GSTREAMER, H264_ALIASES, false, true
                ));
                ll.addView(createCodecTextView(
                    CodecType.WEBRTC, H264_ALIASES, false, true
                ));

            }
        };

        //Gstreamer codecs
        BasGStreamer gStreamer = new BasGStreamer(this);
        gStreamer.getCodecs(this);

        //Android codecs
        handleAndroidCodecs();

        //Webrtc google
        handleWebRTCCodecs();
    }


    void updateUI () {
        runOnUiThread(setUI);
    }

    TextView createCodecTextView(
        CodecType type,
        String[] codecAliases,
        boolean isEncoder,
        boolean isHardwareAccelerated
    ) {

        Codec codec = getCodec(type, codecAliases, isEncoder, isHardwareAccelerated);

        StringBuilder text = new StringBuilder(type.name());
        int color = Color.GREEN;

        if (codec == null) {
            text.append("   (no codec found)");
            color = Color.RED;
        } else if (!codec.hasTypes) {
            text.append("    (codec found, but has no types)");
            color = Color.YELLOW;
        }

        return createTextView(text.toString(), color, TV_HEIGHT_SMALL);
    }

    TextView createTextView(
        String text,
        int color,
        int height
    ) {

        TextView tv = new TextView(this);
        tv.setText(text);
        tv.setHeight(height);
        tv.setTextColor(Color.BLACK);
        tv.setBackgroundColor(color);
        tv.setGravity(Gravity.CENTER);

        return tv;
    }

    //For gathering all codecs available to gstreamer
    @Override
    public void onGStreamerCodec(String codecName) {

        Log.d("gstreamer_codec", codecName);

        allCodecs.add(
            new Codec(
                CodecType.GSTREAMER,
                codecName,
                codecName.contains(VID.toLowerCase()),
                containsHWPrefix(codecName),
                codecName.contains(ENC.toLowerCase()),
                true // no such thing as types in gstreamer
            )
        );

        updateUI();
   }

    /**
     *  For gathering all codecs available to android
     *  This uses the ALL_CODECS enum
     */
    private void handleAndroidCodecs() {

        final MediaCodecList mediaCodecList = new MediaCodecList(MediaCodecList.ALL_CODECS);

        for (final MediaCodecInfo info : mediaCodecList.getCodecInfos()) {

            String codecName = info.getName();

            Log.d("android_codec", codecName);

            allCodecs.add(
                new Codec(
                    CodecType.SYSTEM,
                    codecName,
                    codecName.contains("video"),
                    containsHWPrefix(codecName),
                    info.isEncoder(),
                    info.getSupportedTypes().length > 0
                )
            );
        }

        updateUI();
    }

    //For gathering all codecs available to webrtc library of google
    //This is the Android API with extra steps
    //Codecs with no types are already filtered out by the use of REGULAR_CODECS
    private void handleWebRTCCodecs() {

        EglBase rootEglBase = EglBase.create();
        ArrayList<MediaCodecInfo> webRTCodecs = new HardwareVideoEncoderFactory(
            rootEglBase.getEglBaseContext(),
            true,
            true
        ).getAllCodecs();

        for (final MediaCodecInfo info : webRTCodecs) {

            String codecName = info.getName();

            Log.d("webrtc_codec", codecName);

            allCodecs.add(
                new Codec(
                    CodecType.WEBRTC,
                    codecName,
                    true,
                    containsHWPrefix(codecName),
                    info.isEncoder(),
                    info.getSupportedTypes().length > 0
                )
            );
        }

        updateUI();
    }


    //Check if a list of codecs contains a codec that has all the required
    private Codec getCodec(
        CodecType type,
        String[] codecAliases,
        boolean isEncoder,
        boolean isHardwareAccelerated
    ) {

        for (Codec codec: allCodecs) {

            boolean matchesAlias = false;

            for (String codecType : codecAliases) {
                if (codec.name.toLowerCase().contains(codecType.toLowerCase())) {
                    matchesAlias = true;
                    break;
                }
            }

            if (codec.type.equals(type) &&
                codec.isEncoder == isEncoder &&
                codec.isHardwareAccelerated == isHardwareAccelerated &&
                matchesAlias
            ) return codec;
        }

        return null;
    }

    //Checks if a string contains one of the hw prefixes
    private boolean containsHWPrefix (String string) {

        if (string == null) return false;

        String _string = string.toLowerCase();

        for (String prefix: HARDWARE_IMPLEMENTATION_PREFIXES) {
            String _prefix = prefix.toLowerCase();
            //GStreamer matches without points
            String _prefix_no_dot = _prefix.replace(".","");

            if (_string.contains(_prefix)) {
                return true;
            }
            if (_string.contains(_prefix_no_dot)) {
                return true;
            }
        }

        return false;
    }
}