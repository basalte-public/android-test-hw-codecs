package be.basalte.codectest;

interface GStreamerCodecCallback {
    void onGStreamerCodec(String codec);
}
