package be.basalte.codectest;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.freedesktop.gstreamer.GStreamer;

public class BasGStreamer {
    private static final String TAG = "BasGstreamer";

    private native void nativeInit(String pipeline);
    private native void nativeGetCodecs();
    private static native boolean nativeClassInit();
    private long native_custom_data;

    private Handler mHandler;
    private GStreamerCodecCallback codecCallback;

    static {

        try {
            System.loadLibrary("gstreamer_android");
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "'gstreamer_android' library failed to load: " + e);
        }

        try {
            System.loadLibrary("bas_gstreamer");
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "'libgstreamer_android' library failed to load: " + e);
        }

        nativeClassInit();
    }

    public BasGStreamer(Context context) {

        mHandler = new Handler(Looper.getMainLooper());

        try {
            GStreamer.init(context);
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "GStreamer.init - UnsatisfiedLinkError:" + e);
        } catch (Exception e) {
            Log.e(TAG, "GStreamer.init - Exception:" + e);
        }
    }

    public void getCodecs(GStreamerCodecCallback codecCallback) {

        this.codecCallback = codecCallback;

        try {
            nativeInit("");
            nativeGetCodecs();
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "nativeInit did not link: " + e);
        }
    }

    private void gstreamerCallback(final String message) { }

    private void onMediaSizeChanged(int width, int height) { }

    private void codecCallback(final String codec) {

        if (codecCallback != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    BasGStreamer.this.codecCallback.onGStreamerCodec(codec);
                }
            });
        }
    }
}
