package be.basalte.codectest;

class Codec {

    CodecType type;
    String name;
    boolean isVideo;
    boolean isHardwareAccelerated;
    boolean isEncoder;
    boolean hasTypes;

    public Codec(
        CodecType type,
        String name,
        boolean isVideo,
        boolean isHardwareAccelerated,
        boolean isEncoder,
        boolean hasTypes
    ) {
        this.type = type;
        this.name = name;
        this.isVideo = isVideo;
        this.isHardwareAccelerated = isHardwareAccelerated;
        this.isEncoder = isEncoder;
        this.hasTypes = hasTypes;
    }
}

enum CodecType {
    SYSTEM, GSTREAMER, WEBRTC
}

