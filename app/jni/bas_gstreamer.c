#include <string.h>
#include <stdint.h>
#include <jni.h>
#include <android/log.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <pthread.h>

GST_DEBUG_CATEGORY_STATIC(debug_category);
#define GST_CAT_DEFAULT debug_category

/*
 * These macros provide a way to store the native pointer to CustomData, which might be 32 or 64 bits, into
 * a jlong, which is always 64 bits, without warnings.
 */
#if GLIB_SIZEOF_VOID_P == 8
#define GET_CUSTOM_DATA(env, thiz, fieldID) (CustomData *)(*env)->GetLongField(env, thiz, fieldID)
#define SET_CUSTOM_DATA(env, thiz, fieldID, data) (*env)->SetLongField(env, thiz, fieldID, (jlong)data)
#else
#define GET_CUSTOM_DATA(env, thiz, fieldID) (CustomData *)(jint)(*env)->GetLongField(env, thiz, fieldID)
#define SET_CUSTOM_DATA(env, thiz, fieldID, data) (*env)->SetLongField(env, thiz, fieldID, (jlong)(jint)data)
#endif

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _CustomData
{
  jobject app;                  /* Application instance, used to call its methods. A global reference is kept. */
  GMainContext *context;        /* GLib context used to run the main loop */
  ANativeWindow *native_window; /* The Android native window where video will be rendered */
  pthread_t gst_app_thread;
} CustomData;

/* These global variables cache values which are not changing during execution */
static pthread_key_t current_jni_env;
static JavaVM *java_vm;
static jfieldID custom_data_field_id;
static jmethodID codec_method_id;

/*
 * Private methods
 */

/* Register this thread with the VM */
static JNIEnv *attach_current_thread(void)
{
  JNIEnv *env;
  JavaVMAttachArgs args;

  GST_DEBUG("Attaching thread %p", g_thread_self());
  args.version = JNI_VERSION_1_4;
  args.name = NULL;
  args.group = NULL;

  if ((*java_vm)->AttachCurrentThread(java_vm, &env, &args) < 0)
  {
    GST_ERROR("Failed to attach current thread");
    return NULL;
  }

  return env;
}

/* Unregister this thread from the VM */
static void detach_current_thread(void *env)
{
  GST_DEBUG("Detaching thread %p", g_thread_self());
  (*java_vm)->DetachCurrentThread(java_vm);
}

/* Retrieve the JNI environment for this thread */
static JNIEnv *get_jni_env(void)
{
  JNIEnv *env;

  if ((env = pthread_getspecific(current_jni_env)) == NULL)
  {
    env = attach_current_thread();
    pthread_setspecific(current_jni_env, env);
  }

  return env;
}

static void gst_native_get_codecs(JNIEnv *env, jobject thiz)
{
    GST_DEBUG("Getting codecs");

    CustomData *data = GET_CUSTOM_DATA(env, thiz, custom_data_field_id);
    if (!data)
        return;

    GList* elements = gst_element_factory_list_get_elements
            (GST_ELEMENT_FACTORY_TYPE_ENCODER, GST_RANK_NONE);

    for (GList* l = elements; l; l = l->next) {
        GstElementFactory* f = l->data;

        GST_DEBUG("encoder: %s",GST_OBJECT_NAME(f));

        jstring jmessage = (*env)->NewStringUTF(env, GST_OBJECT_NAME(f));

        (*env)->CallVoidMethod(env, data->app, codec_method_id, jmessage);
        if ((*env)->ExceptionCheck(env))
        {
            GST_ERROR("Failed to call Java method");
            (*env)->ExceptionClear(env);
        }

        (*env)->DeleteLocalRef(env, jmessage);
    }

    elements = gst_element_factory_list_get_elements
            (GST_ELEMENT_FACTORY_TYPE_DECODER, GST_RANK_NONE);

    for (GList* l = elements; l; l = l->next) {
        GstElementFactory* f = l->data;

      GST_DEBUG("decoder: %s",GST_OBJECT_NAME(f));

      jstring jmessage = (*env)->NewStringUTF(env, GST_OBJECT_NAME(f));

        (*env)->CallVoidMethod(env, data->app, codec_method_id, jmessage);
        if ((*env)->ExceptionCheck(env))
        {
            GST_ERROR("Failed to call Java method");
            (*env)->ExceptionClear(env);
        }

        (*env)->DeleteLocalRef(env, jmessage);
    }
}

/*
 * Java Bindings
 */

/* Instruct the native code to create its internal data structure, pipeline and thread */
static void gst_native_init(JNIEnv *env, jobject thiz, jstring address)
{
  CustomData *data = g_new0(CustomData, 1);
  SET_CUSTOM_DATA(env, thiz, custom_data_field_id, data);
  GST_DEBUG_CATEGORY_INIT(debug_category, "BasGstreamer", 0, "BasGstreamer");
  gst_debug_set_threshold_for_name("BasGstreamer", GST_LEVEL_DEBUG);
  GST_DEBUG("Created CustomData at %p", data);
  data->app = (*env)->NewGlobalRef(env, thiz);
  GST_DEBUG("Created GlobalRef for app object at %p", data->app);
}

/* Static class initializer: retrieve method and field IDs */
static jboolean gst_native_class_init(JNIEnv *env, jclass callbackClass)
{
  custom_data_field_id = (*env)->GetFieldID(env, callbackClass, "native_custom_data", "J");
  codec_method_id = (*env)->GetMethodID(env, callbackClass, "codecCallback", "(Ljava/lang/String;)V");

  if (!custom_data_field_id || !codec_method_id)
  {
    /* We emit this message through the Android log instead of the GStreamer log because the later
     * has not been initialized yet.
     */
    __android_log_print(ANDROID_LOG_ERROR, "BasGstreamer", "The calling class does not implement all necessary interface methods");
    return JNI_FALSE;
  }
  return JNI_TRUE;
}

/* List of implemented native methods */
static JNINativeMethod native_methods[] = {
    {"nativeInit", "(Ljava/lang/String;)V", (void *)gst_native_init},
    {"nativeGetCodecs", "()V", (void *)gst_native_get_codecs},
    {"nativeClassInit", "()Z", (void *)gst_native_class_init}};

/* Library initializer */
jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
  JNIEnv *env = NULL;

  java_vm = vm;

  if ((*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_4) != JNI_OK)
  {
    __android_log_print(ANDROID_LOG_ERROR, "BasGstreamer", "Could not retrieve JNIEnv");
    return 0;
  }
  jclass callbackClass = (*env)->FindClass(env,"be/basalte/codectest/BasGStreamer");
  (*env)->RegisterNatives(env, callbackClass, native_methods, G_N_ELEMENTS(native_methods));

  pthread_key_create(&current_jni_env, detach_current_thread);

  return JNI_VERSION_1_4;
}
