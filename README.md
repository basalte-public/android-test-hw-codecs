# android-test-codecs
## Prerequisites

GStreamer biniaries need to be installed.

Download 1.16.3 tarball installer via https://gstreamer.freedesktop.org/data/pkg/android/1.16.3/

Add path like this:

```
    gstAndroidRoot = ~/Downloads/gstreamer-1.16.3
```

to ~/.gradle/gradle.properties

## Summary

This is an android project for checking (hardware) codec support.

Codecs are requested on 3 different ways:
* Android API
* GStreamer
* WebRTC from Google

Hardware codecs can be recognised by there prefix:
* OMX.Exynos.
* OMX.Intel.
* OMX.Nvidia.
* OMX.qcom.

Hardware codecs of lisa and ellie:
* OMX.RK.
* C2.IMX.

## Current status

3 devices were tested: a samsung A40, Ellie and Lisa.
Ellie shows an issues with the VP8 hardware encoder.


The VP8 encoder of Ellie is not included in the `REGULAR_CODECS` list due to missing 'supported types'.
This makes the codec not accessible in WebRTC library and GStreamer.


Android checks the codec capabilities/'supported types' in the `initCodecList` method of the [`MediaCodecList` class](https://android.googlesource.com/platform/frameworks/base/+/master/media/java/android/media/MediaCodecList.java).


The `makeRegular` method of the [`MediaCodecInfo` class](https://android.googlesource.com/platform/frameworks/base/+/master/media/java/android/media/MediaCodecInfo.java) will return null if capabilities/supported types are not recognised or not present.
This is why the VP8 HW encoder codec of Ellie is present in the `ALL_CODECS` but not in the `REGULAR_CODECS` list.

<div style="display: flex; align-items: flex-start; justify-content: space-around">
    <div>
        <div>Ellie</div>
        <img src="screenshots/Ellie.png" alt="drawing" width="200"/>
    </div>
    <div>
        <div>Lisa</div>
        <img src="screenshots/Lisa.png" alt="drawing" width="200"/>
    </div>
    <div>
        <div>A40</div>
        <img src="screenshots/A40.png" alt="drawing" width="200"/>
    </div>
</div>

## Requesting (hardware) codecs

### Android API

The Android API has a build-in class for requesting all available codecs.
See [MediaCodecList](https://developer.android.com/reference/android/media/MediaCodecList)

```
new MediaCodecList(MediaCodecList.ALL_CODECS)
new MediaCodecList(MediaCodecList.REGULAR_CODECS)
```

e.g. 
```
OMX.rk.video_decoder.avc
OMX.google.h264.decoder
...
```
### GStreamer

GStreamer accesses codecs through the androidmedia plugin.
Using a callback the codec names are passed from C to Java code.

```
 GList* elements = gst_element_factory_list_get_elements(GST_ELEMENT_FACTORY_TYPE_ENCODER, GST_RANK_NONE); 
```

e.g. 
```
 amcviddec-omxrkvideodecoderavc
 amcviddec-omxgoogleh264decoder
...
```

### WebRTC from Google

The widely used [webRtc project from google](https://webrtc.github.io/webrtc-org/native-code/android/) also uses the Andoird API for fetching codecs.

This library uses the `REGULAR_CODECS` enum for listing codecs.

```
new MediaCodecList(MediaCodecList.REGULAR_CODECS)

```

e.g. 
```
 OMX.rk.video_decoder.avc
 OMX.google.h264.decoder
...
```

